FROM python:3.11-bullseye
RUN apt-get update && \
    apt-get install curl ruby-full skopeo --yes && \
    curl -sSfL https://raw.githubusercontent.com/anchore/grype/main/install.sh | sh -s -- -b /usr/local/bin && \
    curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/main/contrib/install.sh | sh -s -- -b /usr/local/bin v0.31.2 && \
    pip install -U hoppr==1.8.4 hoppr-cop